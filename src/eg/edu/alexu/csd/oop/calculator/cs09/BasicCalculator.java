package eg.edu.alexu.csd.oop.calculator.cs09;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class BasicCalculator implements Calculator{

		private static Double[] operands;
		private static String[][] History = new String[5][2];
		private static int indicator = 0;
		private static int currentH = 0;
		
		private void changeHistory() {
			
			for(int i=4;i>currentH;i--) {
				History[i][0] = null;
				History[i][1] = null;
			}
			
			if (indicator < 5) {
				History[indicator][0]= DoFormula.toString(operands);
				History[indicator][1]= DoFormula.calculate(operands);
				indicator++;
				currentH = indicator-1;
			}
			
			else {
				for(int i=0;i<4;i++) {
					History[i][0] = History[i+1][0];
					History[i][0] = History[i+1][0];
				}
				indicator = 4;
				changeHistory();
			}
			
		}
	
		public void input(String s) {
			operands = RegexMatches.operate(RegexMatches.validate(s));
		}
	
		public String getResult() {
			changeHistory();
			return History[currentH][1];
		}

		public String current() {
			return History[currentH][0];
		}
	
		public String prev() {
			String s = null;
			
			if(currentH > 0) {
				currentH--;
				s = History[currentH][0];
			}

			return s;
		}
	
		public String next() {
			String s = null;
			
			if(currentH < 4) {
				currentH++;
				s = History[currentH][0];
			}

			return s;
		}
		
		public void save() {
			FilesHandler.saveHistory(History, indicator);
		}
	
		public void load() {
			History = FilesHandler.loadHistory();
			currentH = FilesHandler.lineCounter("History.txt");
			indicator = currentH + 1;	
		}

}
