package eg.edu.alexu.csd.oop.calculator.cs09;

public class DoFormula {
	
	public static String calculate(Double[] operands) {
		
		Double result = operands[0];
		
		   if(operands[2] < 1)
			   	result -= operands[1];
	  else if(operands[2] < 2)
		  		result += operands[1];
	  else if(operands[2] < 3)
		  		result /= operands[1];
	  else if(operands[2] < 4)
		  		result *= operands[1];
		
		return result.toString();
	}
	
	public static String toString(Double[] operands) {
		
		String s = operands[0].toString();
		
		   if(operands[2] < 1)
			   	s += "-";
	  else if(operands[2] < 2)
		  		s += "+";
	  else if(operands[2] < 3)
		  		s += "/";
	  else if(operands[2] < 4)
		  		s += "*";
		
		s += operands[1].toString();
		   
		return s;
	}

}
