package eg.edu.alexu.csd.oop.calculator.cs09;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.Scanner;

public class FilesHandler {

	private static Formatter create;
	
	// Write
	
	private static void openFile() {
		
		try {
			create = new Formatter("History.txt");
		} catch (Exception e) {
			System.out.println("File is missing");
		}
		
	}
	
	private static void addLine(String str) {
		create.format("%s\n",str);
	}
	
	private static void closeFile() {
		create.close();
	}
	
	public static void saveHistory (String[][] History,int indicator) {
		openFile();
		for(int i=0;i<indicator;i++) {
			try {
				addLine(History[i][0].toString() + " = " + History[i][1]);
			}catch (Exception e) {
				System.out.println("error @ i = " + i +" ind =" + indicator);
			}

		}
		closeFile();
	}
	
	// Read
	
	private static Scanner initiateFile(Scanner input, String location) {
		
		try {
			input = new Scanner(new File(location));
		}catch(Exception e){
			System.out.println("File is missing");
		}
		
		return input;
		
	}
	
	public static int lineCounter (String location){
		int lineCount = 0;
		
	    Path path = Paths.get(location);
	    try {
			lineCount = (int) Files.lines(path).count();
		} catch (Exception e) {
			System.out.println("File is missing");
		}
	    
		return lineCount;
		
	}
	
	private static String[] readLine(Scanner input) {
		
		String data = input.nextLine();
		String[] values = data.split(" = ");
		
		return values;
		
	}
	
	private static String[][] readFile(Scanner input, int j, int i) {
		
		String[][] tmpArray = new String[j][i];
		String  [] tmpLine	= new String   [i];
		
		int n=0;
		
		while(input.hasNext()) {
			tmpLine = readLine(input);
			tmpArray[n] = tmpLine;
			n++;
		}
		return tmpArray;
		
	}

	public static String[][] loadHistory() {
		Scanner inputScanner = null;
		String[][] History = new String[5][2];
		
		inputScanner = initiateFile(inputScanner,"History.txt");
		
		History = readFile(inputScanner,5,2);
		
		return History;
	}
	
}
