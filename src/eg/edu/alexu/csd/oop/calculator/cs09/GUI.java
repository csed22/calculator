package eg.edu.alexu.csd.oop.calculator.cs09;
import eg.edu.alexu.csd.oop.calculator.Calculator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class GUI extends JFrame {
	
	private boolean operationDone = false; 
	
	private JTextField typeHere;
	private JLabel resultLabel;
	
	private JButton allowEdit;
	
	private JButton Bclear;
	private JButton Bequal;
	private JButton B0;
	private JButton B1;
	private JButton B2;
	private JButton B3;
	private JButton B4;
	private JButton B5;
	private JButton B6;
	private JButton B7;
	private JButton B8;
	private JButton B9;
	private JButton Bdot;
	private JButton Bplus;
	private JButton Bminus;
	private JButton Bmulti;
	private JButton Bdivide;
	
	private JButton next;
	private JButton prev;	
	private JButton save;
	private JButton load;
	
	GUI(Calculator C){
	
		super("Calculator");
		setLayout(new FlowLayout());
		
		typeHere = new JTextField(10);
		typeHere.setEditable(false);
		add(typeHere);
		
		resultLabel = new JLabel("0.000");
		add(resultLabel);
		
		Bclear = new JButton("C");
		add(Bclear);
		Bclear.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						typeHere.setText(null);							
					}
				}	
			);
		
		B7 = new JButton("7");
		add(B7);
		B8 = new JButton("8");
		add(B8);
		B9 = new JButton("9");
		add(B9);
		Bdivide = new JButton("/");
		add(Bdivide);
		
		B4 = new JButton("4");
		add(B4);
		B5 = new JButton("5");
		add(B5);
		B6 = new JButton("6");
		add(B6);
		Bmulti = new JButton("*");
		add(Bmulti);
		
		B1 = new JButton("1");
		add(B1);
		B2 = new JButton("2");
		add(B2);
		B3 = new JButton("3");
		add(B3);
		Bminus = new JButton("-");
		add(Bminus);
		
		Bequal = new JButton("=");
		add(Bequal);
		B0 = new JButton("0");
		add(B0);
		Bdot = new JButton(".");
		add(Bdot);
		Bplus = new JButton("+");
		add(Bplus);
		
		prev = new JButton("<-");
		add(prev);
		prev.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						typeHere.setText(C.prev());	
						if(RegexMatches.operate(RegexMatches.validate(typeHere.getText()))==null) {}
						else		
							resultLabel.setText(DoFormula.calculate(RegexMatches.operate(RegexMatches.validate(typeHere.getText()))));
					}
				}	
			);
		
		allowEdit = new JButton("type");
		add(allowEdit);
		allowEdit.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						if(allowEdit.getText().equals("type")) {
							allowEdit.setText("lock");
							typeHere.setEditable(true);
						} else {
							allowEdit.setText("type");
							typeHere.setEditable(false);
						}							
					}
				}	
			);
		
		next = new JButton("->");
		add(next);
		next.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						typeHere.setText(C.next());	
						if(RegexMatches.operate(RegexMatches.validate(typeHere.getText()))==null) {}
						else
							resultLabel.setText(DoFormula.calculate(RegexMatches.operate(RegexMatches.validate(typeHere.getText()))));
					}
				}	
			);
		
		save = new JButton("save");
		add(save);
		save.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						C.save();						
					}
				}	
			);
		load = new JButton("load");
		add(load);
		load.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						C.load();					
					}
				}	
			);
		
		HandlerClass typeIt = new HandlerClass();
		
		B0.addActionListener(typeIt);		
		B1.addActionListener(typeIt);	
		B2.addActionListener(typeIt);	
		B3.addActionListener(typeIt);	
		B4.addActionListener(typeIt);	
		B5.addActionListener(typeIt);	
		B6.addActionListener(typeIt);	
		B7.addActionListener(typeIt);	
		B8.addActionListener(typeIt);	
		B9.addActionListener(typeIt);	
		Bdot.addActionListener(typeIt);	
		Bplus.addActionListener(typeIt);	
		Bminus.addActionListener(typeIt);	
		Bmulti.addActionListener(typeIt);	
		Bdivide.addActionListener(typeIt);
		
		Bequal.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						String s = typeHere.getText();
						if (RegexMatches.operate(RegexMatches.validate(s)) == null) {
							JOptionPane.showMessageDialog(null, "Wrong Formula!");
							typeHere.setText(null);
						} else {
							C.input(s);
							resultLabel.setText(C.getResult());
							operationDone = true;
						}
					}
				}	
			);
		
	}
	
	private class HandlerClass implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			if(operationDone) {
				typeHere.setText(null);
				operationDone = false;
			}
			
			String s = typeHere.getText();
			s += event.getActionCommand();
			typeHere.setText(s);
		}
		
	}
	
	public static void act(Calculator C) {
		
		GUI calculatorGUI = new GUI(C);
		calculatorGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calculatorGUI.setLocationRelativeTo(null);
		calculatorGUI.setSize(230,260);
		calculatorGUI.setResizable(false);
		calculatorGUI.setVisible(true);
		
	}
	

}
