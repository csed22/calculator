package eg.edu.alexu.csd.oop.calculator.cs09;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMatches {
	
	public static String[] validate ( String line ) {
	   
		String[] expression = new String[5];
		
		// String to be scanned to find the pattern.
		String pattern = "\\s*([-+])?\\s*(([\\d]+)?[.]?([\\d]+))(?:\\s*([-+*\\/])\\s*([-+])?\\s*(([\\d]+)?[.]?([\\d]+))\\s*)";
		
		// 9 groups, neglecting group 0
		
		/*
		 * group 1: [optional?] - or +
		 * group 2: The FIRST number
		 * group 3: (redundant) integer part of the first number
		 * group 4: (redundant) fractional part of the first number
		 * group 5: the OPERATION to be done
		 * group 6: [optional?] - or +
		 * group 7: The SECOND number
		 * group 8: (redundant) integer part of the second number
		 * group 9: (redundant) fractional part of the second number
		 */
		
		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);
		
		// Now create matcher object.
		Matcher m = r.matcher(line);
		
		if (m.find( )) {
			expression[0] = m.group(1);
			expression[1] =	m.group(2);
			expression[2] = m.group(5);
			expression[3] =	m.group(6);
			expression[4] =	m.group(7);
							
		  //System.out.println( m.group(1) + m.group(2) + " " + m.group(5) + " " + m.group(6) + m.group(7));
			
			/*
			System.out.println("Found value: " + m.group(0) );
			System.out.println("Found value: " + m.group(1) );
			System.out.println("Found value: " + m.group(2) );
			System.out.println("Found value: " + m.group(3) );
			System.out.println("Found value: " + m.group(4) );
			System.out.println("Found value: " + m.group(5) );
			System.out.println("Found value: " + m.group(6) );
			System.out.println("Found value: " + m.group(7) );
			System.out.println("Found value: " + m.group(8) );
			System.out.println("Found value: " + m.group(9) );
			*/
		}else{
			return null;
		}
		
		return expression;
	}
	
	public static Double[] operate ( String[] expression ) {
		if (expression==null) {
			//System.out.println("WRONG EXPRESSION");
			return null;
		}
		
		Double[] operands = new Double[3];
		
		operands[0] = Double.parseDouble(expression[1]);
		if(expression[0] == null) {}
		else if(expression[0].equals("-"))
			operands[0] = -1 * operands[0];
		
		operands[1] = Double.parseDouble(expression[4]);
		if(expression[3] == null) {}
		else if(expression[3].equals("-"))
			operands[1] = -1 * operands[1];
		
			if(expression[2].equals("-"))
				 operands[2] = 0.9;
	   else if(expression[2].equals("+"))
				 operands[2] = 1.9;
	   else if(expression[2].equals("/"))
			 	 operands[2] = 2.9;
	   else if(expression[2].equals("*"))
		   		 operands[2] = 3.9;
		
		return operands;		
	}
   
}

//	https://www.tutorialspoint.com/java/java_regular_expressions.htm
//	https://stackoverflow.com/questions/24234265/how-to-use-regular-expression-for-calculator-input-with-javascript
//	tested the regular expression here -> https://regex101.com/r/oy4Fiq/3
